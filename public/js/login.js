(function ($){

    $("#connection").on('click', ' #LoginUser', function (e) {
        $("#exampleModalCenter").modal();
        var data = $('#connection').serialize();
        $.ajax({
            url: '/home/login',
            type: 'post',
            data: data
        })
            .done(function (data, text, jqxhr) {
                var rest = JSON.parse(data);

                if(rest['result'] === true){
                    //$("#exampleModalCenter").modal("hide");
                    window.location.href = "/Admin/dashbord";
                }else{
                    alert('Incorrect');
                }
                
            })
            .fail(function (jqxhr) {
                alert(jqxhr.responseText)
            })
            .always(function () {
                
            })
    });

})(jQuery);