(function ($) {

    $('#Delete-product').on('click', '.btn-link.text-danger', function (e) {
        e.preventDefault();
        var $a = $(this);
        var url = $a.attr('href');
        $.ajax(url)
            .done(function (data, text, jqxhr) {
                var rest = JSON.parse(data);

                if (rest['result'] !== true) {
                    Swal.fire({
                        title: 'Erreur',
                        type: 'warning',
                        text: rest['message'],
                        showCancelButton: false,
                        focusConfirm: false,
                        confirmButtonText:
                            'OK!',
                        confirmButtonAriaLabel: 'Product insert successs!',
                    }).then((result) => {
                        if (result.value) {
                            if (rest['result'] === true)
                                window.location.href = "/Home/add";
                        }
                    })
                }else{
                    $a.parents('tr').fadeOut();
                }
            })
            .fail(function (jqxhr) {
                alert(jqxhr.responseText)
            })
            .always(function () {
                //$a.text('Delete load ...');
            })
    });


    $("#addProduct").submit(function (e) {
        e.preventDefault()
        var data = $('#SubmitForm').serialize()
        $.ajax({
            type: "post",
            url: '/Admin/dashbord/SaveData',
            data: data,
            success: function (result) {

                var rest = JSON.parse(result);
                var msg = 'Product insert succefully';
                var type = 'success'
                var title = 'Success'
                console.log(rest);

                if (rest['result'] !== true) {
                    msg = rest['message']
                    type = 'warning'
                    title = 'Erreur'
                }

                Swal.fire({
                    title: title,
                    type: type,
                    text: msg,
                    showCancelButton: false,
                    focusConfirm: false,
                    confirmButtonText:
                        'OK!',
                    confirmButtonAriaLabel: 'Product insert successs!',
                }).then((result) => {
                    if (result.value) {
                        if (rest['result'] === true)
                        {
                            console.log(rest);
                            if(rest['redirect']){
                                if(rest['redirect'] === true)
                                    window.location.href = "/Home/add";
                            }else{
                                window.location.href = "/Admin/dashbord/add";
                            }
                        }
                    }
                })
            },
            error: function () {
                alert('error!');
            }
        })
    });

})(jQuery);








