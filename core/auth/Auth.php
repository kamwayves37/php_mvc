<?php
namespace Core\Auth;

class Auth{

    private static $key_verified = '5a6f2758442389b09@c4caa9380b20dcc';

    private static function check_status(){
        if(session_status() === PHP_SESSION_NONE)
            session_start();
    }

    public static function isConnect(){
        self::check_status();
        if(isset($_SESSION['Auth']) && !empty($_SESSION['Auth']) && isset($_SESSION['Status'])
             && $_SESSION['Status'] === self::$key_verified){
            return true;
        }
        return false;
    }

    public static function checkedToken($token=''){
        if(isset($_SESSION['Auth']) && !empty($_SESSION['Auth']) && 
            isset($_SESSION['Token']) && !empty($_SESSION['Token']) && !empty($token)){
            return $_SESSION['Token'] === $token;
        }

        return false;
    }

    public static function getToken(){
        if(isset($_SESSION['Token']) && !empty($_SESSION['Token']))
            return $_SESSION['Token'];
    }

    public static function connect($data){
        self::check_status();
        if(!isset($_SESSION['Auth'])){
            $_SESSION['Auth'] = $data;
            $_SESSION['Token'] = self::generedToken();
            $_SESSION['Status'] = self::$key_verified;
        }    
        else
            echo 'Utilisateur déja connecté';
    }

    public static function deconnect(){
        self::check_status();
        $_SESSION = array();
    }

    private static function generedToken($option=''){
        return md5(time() .rand(181,696). $option);
    }

    public static function cryptPassword($param){
        return crypt($param,password_hash($_POST[$param],PASSWORD_BCRYPT));
    }

    public static function decryptPassword($password,$param){
        return hash_equals($password,crypt($param,$password));
    }

}