<?php

namespace Core\Model;

use Core\Database\Database;

abstract class Model
{
    private static $_bdd;

    private static function setBdd()
    {
        define('server','localhost');
        define('db_name','tuto');
        define('user','root');
        define('password','');

        self::$_bdd = Database::mysqlConnect(server,db_name,user,password);
    }

    protected function getBdd()
    {
        if(self::$_bdd == null)
        self::setBdd();

        return self::$_bdd;
    }

    private static function verified_obj($obj){
        if(!class_exists($obj)){
            echo "La Classe <b> '.$obj.'</b> n'est pas défini ou inexistant";
            return null;
        }  
    }

    //DEFINITION DES FUNCTIONS

    protected function login($user_name,$table, $obj,$clause){
        self::verified_obj($obj);

        if(self::$_bdd){
            try{
                $req = self::$_bdd->prepare('SELECT * FROM ' .$table. ' WHERE '. $clause . '=?');
                $req->setFetchMode(\PDO::FETCH_CLASS, $obj);
                $req->execute([$user_name]);
                return $req->fetch();

            }catch(\Exception $e){
                echo $e->getMessage();
            }finally{
                $req->closeCursor();
            }
        }else{
            echo 'Aucun connexion lié a la base de donnée';
        }
    }

    protected function getAll($table, $obj,$clause = [])
    {
        if(!class_exists($obj)){
            echo "La Classe <b> '.$obj.'</b> n'est pas défini ou inexistant";
            return null;
        }

        if(self::$_bdd){
            try{
                $clause = !empty($clause) ? 'ORDER BY '.implode(',',$clause) : '';
                $var = [];
                $req = self::$_bdd->prepare('SELECT * FROM ' .$table. ' '. $clause);
                $req->execute();
                if($req->rowCount() > 0){
                    while($data = $req->fetch(\PDO::FETCH_ASSOC))
                    {
                        $var[] = new $obj($data);
                    }
                    
                }
                
                return $var;

            }catch(\Exception $e){
                echo $e->getMessage();
            }finally{
                $req->closeCursor();
            }
        }else{
            echo 'Aucun connexion lié a la base de donnée';
        }
    }

    protected function get($table, $obj, $cond=''){
        if(!class_exists($obj)){
            echo "La Classe <b> '.$obj.'</b> n'est pas défini ou inexistant";
            return null;
        }
        if(self::$_bdd){
            try {
                $req = self::$_bdd->query('SELECT * FROM ' .$table. ' WHERE ' .$cond. ' ORDER BY id desc');
                $req = $req !== false ? $req->fetch() : null ;
                return ($req ? new $obj($req) : null);
            } catch(\Exception $e){
                echo $e->getMessage();
            }
        }else{
            echo 'Aucun connexion lié a la base de donnée';
        }   
    }

    protected function select($table, $obj = '', array $projection = [], array $condition = [],array $expression = [], $clause = []){
        if(!class_exists($obj)){
            echo "La Classe <b> '.$obj.'</b> n'est pas défini ou inexistant";
            return null;
        }

       if(self::$_bdd){

            try {

                $projection = !empty($projection) ? implode(',',$projection) : '*';
                $condition = !empty($condition) ? 'WHERE '.implode(' AND ',$condition) : '';
                $expression = !empty($expression) ? 'GROUP BY '.implode(',',$expression) : '';
                $clause = !empty($clause) ? 'ORDER BY '.implode(',',$clause) : '';

                $query = 'SELECT ' .$projection. ' FROM ' .$table. ' '.$condition.' '.$expression. ' ' .$clause .' LIMIT 10;';

                $var = [];
                $req = self::$_bdd->prepare($query);
                $req->execute();

                if($req->rowCount() > 0){
                    while($data = $req->fetch(\PDO::FETCH_ASSOC))
                    {
                        $var[] = new $obj($data);
                    }
                    self::Test($var);
                    $req->closeCursor();
                    return $var;
                }else{
                    echo 'Aucun élément selectionné';
                }

            } catch(\Exception $e){
                echo $e->getMessage();
            }          
        }else{
            echo 'Aucun connexion lié a la base de donnée';
        }
    }

    protected function insert($data = [], $remove=''){ 
       
        if(self::$_bdd){
            if(!empty($data)){
                try {
                    $table = lcfirst($data->get_name());
                    $varName = $data->get_vars();
                    unset($varName[$remove]);
                    $key = array_keys($varName);
                    $values = array_values($varName);
                    $result = [];
                    foreach($values as $k => $val){
                            if(is_string($val))
                                $values[$k] = self::$_bdd->quote($val);
                    }
    
                    $query = 'INSERT INTO ' .$table. '('. implode(',',$key) .') VALUES'.'('. implode(',',$values) .')';
                    $req = self::$_bdd->prepare($query);
                    $result["result"]  = $req->execute();
                    $result["message"] = 'Succed';
                    $req->closeCursor();
    
                } catch(\Exception $e){
                    $result["result"] = false;
                    $result["message"] = $e->getMessage();
                }finally{
                    return $result;
                }
    
            }else{
                return false;
            }
        }else{
            $result["result"] = false;
            $result["message"] = "Erreur de connexion avec la base de donnée";
            return $result;
        }
        
    }

    protected function delete($table,$cond = ''){
        if(self::$_bdd){
            try {
                $query = 'DELETE FROM ' .$table. ' WHERE ' .$cond;
                $result = [];
                $req = self::$_bdd->prepare($query);
                $result["result"]  = $req->execute();
                $result["message"] = 'Succed Delete';
                $req->closeCursor();
            } catch(\Exception $e){
                $result["result"] = false;
                $result["message"] = $e->getMessage();
            } finally{
                return $result;
            }
        }else{
            echo 'Aucun connexion lié a la base de donnée';
        }  
    }

    protected function update($data = [], $ignore='' , $remove=''){
        if(self::$_bdd){
            try {
                $table = lcfirst($data->get_name());
                $params = $data->get_vars();
                unset($params[$remove]);
                $key = array_keys($params);
                $values = array_values($params);
                $keyIgnore = -1;
                $formatter = [];

                foreach($values as $k => $val){
                    if(is_string($val))
                        $val = self::$_bdd->quote($val);

                    $formatter[] = $key[$k].'='.$val;
                    if($key[$k]==$ignore)
                        $keyIgnore = $k;
                }

                $ignore = $formatter[$keyIgnore];
                unset($formatter[$keyIgnore]); 

                $query = 'UPDATE ' .$table. ' SET ' .implode(',',$formatter). ' WHERE '. $ignore;
                $req = self::$_bdd->prepare($query);
                $result["result"] = $req->execute();
                $result["message"] = 'Succed';
                $result["redirect"] = false;

            } catch (\Exception $ex) {
                $result["result"] = false;
                $result["message"] = $ex->getMessage();
            }finally{
                return $result;
            }

        }else{
            $result["result"] = false;
            $result["message"] = "Erreur de connexion avec la base de donnée";
            return $result;
        }
        
    }

    private function Test($var){
        echo('<pre>');
        print_r($var);
        echo('</pre>');
    }

}