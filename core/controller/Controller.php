<?php 

namespace Core\Controller;

use Core\View\View;

abstract class Controller
{
    protected $view;
    protected $model;

    public function view($viewName, $data=[],$template='layout'){
        $this->view = new View($viewName,$data,$template);
        return $this->view;
    }

    public function model($modelName){
        try{
            if(file_exists(MODEL . $modelName .'.php')){
                require_once(MODEL . $modelName .'.php');
                $this->model = $modelName::getInstance();
            }else{
                throw new Exception('<b>Model: ' .$modelName.'</b> est introuvable dans le repertoire spécifié');
            }
        }catch(Exception $e){
            echo $e->getMessage();
        }
    }
} 