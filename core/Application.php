<?php

namespace Core;

class Application
{
    protected $controller = '';
    protected $action = '';
    protected $prams = [];
    private $_isAdmin = false;

    public function __construct()
    {
        try{
            $this->prepareURL();
        //echo $this->controller,'<br>', $this->action,'<br>',  print_r($this->prams);

            if(file_exists(CONTROLLER . $this->controller . '.php') || ($this->_isAdmin === true && file_exists(ADMIN . $this->controller . '.php'))){
                
                $this->controller = new $this->controller;

                if(method_exists($this->controller, $this->action)){
                    call_user_func_array([$this->controller, $this->action],$this->prams);
                }else{
                    throw new \Exception("<b>Action: ". $this->action."</b> n'est pas définir");
                }
            }else{
                throw new \Exception("<b>Controller: ".$this->controller."</b> n'est pas définir");
            }

        }catch(\Exception $e){
            echo $e->getMessage();
        }
    }

    protected function prepareURL(){
        
        $request = trim($_SERVER['REQUEST_URI'], '/');        
        if(!empty($request))
        {
            $url = explode('/', trim($request,'/'));

            //Check administrator
            $id = 0;
            if(isset($url[0]) && $url[0] === "Admin"){
                $this->_isAdmin = true;
                $id = 1;
            }

            $this->controller = isset($url[0]) ? explode('?', $url[$id])[0].'Controller' : 'homeController';

            $post = isset($url[$id+1]) ? explode('?', $url[$id+1])[0] : 'index';

            $this->action = $post;

            if(isset($url[0]) && $url[0] === "Admin")
                unset($url[0],$url[1],$url[2]);
            else
                unset($url[0],$url[1]);

            if(!empty($url)) end($url);

            $url[key($url)] = explode('?', end($url))[0];
            
            $this->prams = !empty($url) ? array_values($url) : [];

        }else{
            $this->controller = 'homeController';
            $this->action = 'index';
        }
    }
}