<?php

    namespace Core\Database;

    class Database{
        public static function mysqlConnect($server,$db_name,$user,$password){
            try{
                $connection = new \PDO("mysql:host=".$server.";dbname=".$db_name,$user,$password);
                $connection->setAttribute(\PDO::ATTR_ERRMODE, \PDO::ERRMODE_EXCEPTION);
                return $connection;
            }catch(Exception $e){
                die("Connection Error:". $e->getMessage());
            }
        }
    } 

?>