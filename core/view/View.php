<?php

namespace Core\View;

class View
{
    protected $view_file;
    protected $view_data;
    protected $_t;
    private $template_name;

    public function __construct($view_file,$view_data,$template)
    {
        $this->view_file = $view_file;
        $this->view_data = $view_data;
        $this->template_name = $template;
        
    }

    public function render(){
        if(file_exists(VIEW . $this->view_file . '.php')){
            $content = $this->generateFile(VIEW . $this->view_file . '.php', $this->view_data);

            $view = $this->generateFile(VIEW .'template'. DIRECTORY_SEPARATOR .$this->template_name. '.php', array('t' => $this->_t,
            'content' => $content));

            echo $view;

        }else{
            throw new Exception($this->view_file.' est introuvable dans le repertoire spécificque');
        }
    }

    private function generateFile($file, $data)
    {
        if(file_exists($file))
        {
            extract($data);

            ob_start();

            require $file;

            return ob_get_clean();
        }
        else
        {
            throw new Exception('Fichier '.$file.' introuvable');
        }
    }
}