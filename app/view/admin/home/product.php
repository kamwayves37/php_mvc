<?php
 $products =  array_key_exists('products',$this->view_data)  ? $this->view_data['products'] !== null ? $this->view_data['products'] :[] : [];
 $token =  array_key_exists('token',$this->view_data)  ? $this->view_data['token'] : '?';
 //var_dump($this->view_data);

 ?>
<div class="position-relative overflow-hidden p-3 p-md-5 m-md-3 bg-light" id="product">
              <div class="panel panel-default">
              <div class="card">
                <div class="card-body" style="padding: 1rem 1rem 3.5rem;height: 60px;">
                  <div class="row">
                    <div class="col">
                      <div class="panel-heading">
                      <h3 class="panel-title">Latest Product</h3>
                      </div>
                    </div>
                    <div class="col text-right">
                      <a href="/Admin/dashbord/add" class="btn btn-link" >Add</a>
                    </div>
                  </div>
                    
                </div>
                </div>
              </div>
              <div class="panel panel-default" style="margin-top:10px;">
                
                <div class="panel-body">
                  <table class="table table-striped " id="Delete-product">
                      <tr>
                        <th>Name</th>
                        <th>Price</th>
                        <th>Quantity</th>
                        <th>Status</th>
                        <th>Action</th>
                      </tr>
                      
                      <?php foreach($products as $product): ?>
                      <tr>
                        <td><?= $product->name(); ?></td>
                        <td><?= $product->price(); ?></td>
                        <td><?= $product->quantity(); ?></td>
                        <td>
                            <?php 
                                if($product->status() === 1)
                                    echo 'En promo';
                                else
                                    echo 'Sans reduction' ?>
                        </td>
                        <td>
                            <div class="form-group" style="margin-bottom: 0;">
                                <a href="/Admin/dashbord/detail/<?= $token ?>/<?= $product->id(); ?>" class="btn btn-link">Detail</a>
                                <a href="/Admin/dashbord/edit/<?= $product->id(); ?>" class="btn btn-link">Edit</a>
                                <a href="/Admin/dashbord/delete/<?= $token ?>/<?= $product->id(); ?>" class="btn btn-link text-danger">Delete</a>
                            </div>
                        </td>
                      </tr>
                    <?php endforeach ?>  
                    </table>
                </div>
              </div>
              
          </div>