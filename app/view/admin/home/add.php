<?php
    $product =  array_key_exists('product',$this->view_data)  ? $this->view_data['product'] : null;
    $token =  array_key_exists('token',$this->view_data)  ? $this->view_data['token'] : '';
 ?>

<div class="position-relative overflow-hidden p-3 p-md-5 m-md-3 bg-light">
    <form id="addProduct" method="post" role="Edit" data-target="<?php if($product!=null) echo '1'; else echo '2'; ?>">
        <fieldset id="SubmitForm">
            <div class="form-group">
                <label for="inputEmail4">Name</label>
                <input type="text" class="form-control" name="name"  placeholder="Name" value="<?php if($product!=null) echo $product->name(); ?>" required>
            </div>
            <div class="form-group">
                <label for="inputPassword4">Price</label>
                <input type="number" class="form-control" name="price"  placeholder="Number" value="<?php if($product!=null) echo $product->price(); ?>">
            </div>
            <div class="form-group">
                <label for="inputPassword4">Quantity</label>
                <input type="number" class="form-control" name="quantity"  placeholder="Quantity" value="<?php if($product!=null) echo $product->quantity(); ?>">
            </div>
            <div class="form-group">
                <label for="inputPassword4">Status</label>
                <input type="number" class="form-control" name="status" placeholder="Status" value="<?php if($product!=null) echo $product->status(); ?>">
            </div>
            <div class="form-group text-center">
               <input type="hidden" name="token" value="<?= $token ?>">
               <button type="submit" class="btn btn-primary btn-block"><?php if($product!=null) echo 'Update'; else echo 'Save'?></button>
            </div>
        </fieldset>
    </form>
</div>
