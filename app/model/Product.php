<?php

class Product
{
    private $id = 0;
    private $name;
    private $price = 0;
    private $quantity = 0;
    private $status = 0;

    public function __construct(array $data)
    {
        $this->hydrate($data);
    }

    public function hydrate(array $data)
    {
        foreach($data as $key => $value)
        {
            $method = 'set'.ucfirst($key);

            if(method_exists($this, $method))
                $this->$method($value);
        }
    }

    public function get_vars(){
        return get_object_vars($this);
    }

    public function get_name(){
        return __CLASS__;
    }

    public function setId($id)
    {
        $id = (int) $id;

        if($id > 0)
            $this->id = $id;
    }

    public function setName($name)
    {
        if(is_string($name))
        $this->name = $name;
    }

    public function setPrice($price)
    {
        $price = (double) $price;

        if($price > 0)
            $this->price = $price;
    }

    public function setQuantity($quantity)
    {
        $quantity = (double) $quantity;

        if($quantity > 0)
            $this->quantity = $quantity;
    }

    public function setStatus($status)
    {
        $status = (int) $status;

        if($status > 0)
            $this->status = $status;
    }

    public function id()
    {
        return $this->id;
    }

    public function name()
    {
        return $this->name;
    }

    public function price()
    {
        return $this->price;
    }

    public function quantity()
    {
        return $this->quantity;
    }

    public function status()
    {
        return $this->status;
    }
}