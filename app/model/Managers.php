<?php

use Core\Model\Model;

class Managers extends Model
{
    private static $_instance;

    public function loginUser($user_name){
        $this->getBdd();
        return $this->login($user_name,'user', 'User','email');
    }

    public function __construct()
    {
        $this->id = uniqid();        
    }

    //GET INSTANCE OD THE CLASS
    public static function getInstance(){
        if(is_null(self::$_instance)){
            self::$_instance = new Managers();
        }
        return self::$_instance;
    }

    public function getProducts()
    {
        $this->getBdd();

        //$this->selectProduct('product',[],['price>0'],['name','id'],['name ASC']);

        return $this->getAll('product', 'Product');
    }

    public function getProduct($cond)
    {
        $this->getBdd();
        return $this->get('product', 'Product',$cond);
    }

    public function selectProduct($table, array $projection, array $condition,array $expression,$clause){

        $this->getBdd();
        $this->select($table,"Product", $projection,$condition,$expression,$clause);
    }

    public function insertProduct(array $data){
        if(!empty($data)){
            $this->getBdd();
            return $this->insert(new Product($data),'price');
        }
    }

    public function deleteProduct($cond){
        $this->getBdd();
        return $this->delete('product', $cond);
    }

    public function updateProduct(array $data,$s){
        if(!empty($data)){
            $this->getBdd();
            $pro = new Product($data);
            $pro->setId($s);
            return $this->update($pro,'id');
        }
    }
}