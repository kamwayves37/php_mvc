<?php
use Core\Controller\Controller;
use Core\Auth\Auth;

class dashbordController extends Controller
{
    public function __construct(){
        //Check a user connect
        if(!Auth::isConnect())
            header('location:/home');
    }

    public function index(){
        $this->view('admin' .DIRECTORY_SEPARATOR. 'home' .DIRECTORY_SEPARATOR. 'index',[],'admin_layout');
        $this->view->page_title = 'Home';
        $this->view->render();
    
    }

    public function product(){
        $this->model('Managers');

        $this->view('admin' .DIRECTORY_SEPARATOR. 'home' .DIRECTORY_SEPARATOR. 'product',['products' => $this->model->getProducts(),'token' => Auth::getToken()],'admin_layout');
        $this->view->page_title = 'Product';
        $this->view->render();
    
    }

    public function aboutUs($id='', $name=''){
        echo 'ID: '.$id.' NAME: '.$name;
        $this->view('admin' .DIRECTORY_SEPARATOR. 'home' .DIRECTORY_SEPARATOR. 'aboutUs',[
            'name' => $name,
            'id' => $id
        ],'admin_layout');

        $this->view->page_title = 'About';
        $this->view->render();
        
    }

    public function add(){
        $this->view('admin' .DIRECTORY_SEPARATOR. 'home' .DIRECTORY_SEPARATOR. 'add',['token' => Auth::getToken()],'admin_layout');
        $this->view->page_title = 'Add';
        $this->view->render();
    }


    public function edit($id=''){
        $this->model('Managers');
        $this->view('admin' .DIRECTORY_SEPARATOR. 'home' .DIRECTORY_SEPARATOR. 'add',['product' => $this->model->getProduct('id='.$id),'token' => Auth::getToken()],'admin_layout');
        $this->view->page_title = 'Edit';
        $this->view->render();
    }

    public function req_uri($val){
        $param  = explode('/', trim($val, '/'));
        $param = explode('?',end($param))[0];
        return $param;
    }

    public function saveData(){
        if(!empty($_POST)){
            if(isset($_SERVER['HTTP_X_REQUESTED_WITH']) &&
            !empty($_SERVER['HTTP_X_REQUESTED_WITH']) &&
            strtolower($_SERVER['HTTP_X_REQUESTED_WITH']) == 'xmlhttprequest'){   

            if(isset($_POST['token']) && Auth::checkedToken($_POST['token'])){
                $result = [];
                $req_type = $this->req_uri($_SERVER['HTTP_REFERER']);
                $this->model('Managers');
                if($req_type === 'add'){
                    $result = $this->model->insertProduct($_POST);
                }else if(is_numeric($req_type)){         
                    $result = $this->model->updateProduct($_POST,$req_type);
                }else{
                    $result["result"] = false;
                    $result["message"] = 'Option faile';
                }
                echo json_encode($result);
            }
            else
                echo json_encode(['result' => false, "message" => "Token espiré ou invalide"]);
            
        }
        }else{
            echo 'ERREUR 404';
        }
    }

    public function delete($token='', $id=-1){
        if(isset($_SERVER['HTTP_X_REQUESTED_WITH']) &&
        !empty($_SERVER['HTTP_X_REQUESTED_WITH']) &&
        strtolower($_SERVER['HTTP_X_REQUESTED_WITH']) == 'xmlhttprequest'){

            if(Auth::checkedToken($token)){
                $id = (int)$id;
                $this->model('Managers');
                $result = $this->model->deleteProduct('id='.$id);
                echo json_encode($result);
            }
            else
                echo json_encode(['result' => false, "message" => "Token espiré ou invalide"]);
        }else{
            echo 'ERREUR 404';
        }
    }

    public function logout(){
        Auth::deconnect();
        header('location:/home');
    }
}