<?php
use Core\Controller\Controller;
use Core\Auth\Auth;

class homeController extends Controller{

    public function index(){
        $this->view('home' .DIRECTORY_SEPARATOR. 'index');
        $this->view->page_title = 'Shop maket';
        $this->view->render();
    }

    public function login(){
      if(!empty($_POST)){
        if(isset($_SERVER['HTTP_X_REQUESTED_WITH']) &&
        !empty($_SERVER['HTTP_X_REQUESTED_WITH']) &&
            strtolower($_SERVER['HTTP_X_REQUESTED_WITH']) == 'xmlhttprequest'){   

                $this->model('Managers');
                $user = $this->model->loginUser($_POST['login']);
                if(!is_bool($user) && isset($_POST['login']) && !empty($_POST['password']))
                    $rslt = Auth::decryptPassword($user->password,$_POST['password']);
                else
                    $rslt = false;
                    
                if($rslt === true)
                    Auth::connect(['id' => $user->id, 'name' => $user->lastname]);

                echo json_encode(['result' => $rslt]);
            }
        }else{
            echo 'ERREUR 404';
        }
    }
}